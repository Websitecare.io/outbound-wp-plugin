<?php
/**
 * A class that checks for updates and loads them if required.
 * As of now the updates are based on the gitlab release tag.
 * 
 * @todo update to a never version of the plugin?
 * @see https://github.com/YahnisElsts/plugin-update-checker
 * 
 * @todo rename methods not to use heylink_prefix
 * 
 * @package WordPress
 * @subpackage Heylink Tracking
 */
class Heylink_Update_Checker {

	/**
	 * Repository link.
	 * 
	 * @var string 
	 */
	public static $repository_link = 'https://gitlab.com/heylink-public/outbound-wp-plugin/';

	/**
	 * Require an external library to perform the updates. Hook into WordPress update / upgrade process
	 * to fetch plugin from a custom, external URL, in this case a gitlab repository.
	 */
	public function __construct() {

		require (plugin_dir_path( __FILE__ ) . 'update/plugin-update-checker.php');
		Puc_v4_Factory::buildUpdateChecker( Heylink_Update_Checker::$repository_link, __DIR__ . DIRECTORY_SEPARATOR . 'heylink.php', 'heylink' );

		/**
		 * Plugin status route.
		 */
		if ( !function_exists( 'get_plugin_data' ) ) {
			require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		}

		/**
		 * @todo maybe make it a normal function, not anonymous / closure
		 */
		add_action( 'rest_api_init', function() {
			register_rest_route( 'heylink', '/status', [
					'methods' => 'GET',
					'callback' => array( $this, 'heylink_tracker_status' ),
					'permission_callback' => __return_true(),
			] );
		} );

		add_action( 'upgrader_process_complete', array( $this, 'heylink_tracker_update_callback' ), 10, 2 );
		add_filter( 'pre_update_option_heylink_tracker_plugin_options', array( $this, 'heylink_tracker_plugin_clear_cache_notification_show' ), 10, 3 );
	}

	/**
	 * Sets latest plugin version used with automatic updates.
	 * 
	 * @global type $wp_version
	 * @param WP_REST_Request $request
	 * @return type
	 */
	public function heylink_tracker_status( WP_REST_Request $request ) {
		$plugin_data = get_plugin_data( __DIR__ . DIRECTORY_SEPARATOR . 'heylink.php' );
		global $wp_version;

		$allOptions = get_option( 'heylink_tracker_plugin_options' );
		$allOptions['wp_version'] = $wp_version;
		$allOptions['plugin_version'] = $plugin_data['Version'];

		return $allOptions;
	}

	/**
	 * Plugin update callback, as in what files to overwrite and where.
	 * 
	 * @hook upgrader_process_complete
	 */
	public function heylink_tracker_update_callback( $upgrader_object, $hook_extra ) {
		foreach ( $hook_extra['plugins'] as $plugin_rel_path ) {
			if ( $plugin_rel_path == 'heylink-plugin/heylink.php' ) {
				// it works only if plugin folder named "heylink" and main plugin file named "heylink.php"
				// if it changes you should change a path 'heylink/heylink.php' in string above
				$plugin_data = get_plugin_data( WP_PLUGIN_DIR . "/$plugin_rel_path" );
			}
		}
	}

	/**
	 * @todo not sure if this is needed and what it does
	 * 
	 * @hook pre_update_option_heylink_tracker_plugin_options
	 */
	public function heylink_tracker_plugin_clear_cache_notification_show( $value, $old_value, $option ) {
		update_option( 'heylink_tracker_plugin_clear_cache_notification', 'show' );
		return $value;
	}

}

$heylink_update_checker = new Heylink_Update_Checker();
