<?php
/**
 * A class that adds a number of configurable plugin options
 * within the WordPress Dashboard.
 * 
 * @todo maybe rename the method names, they don't need a heylink_ prefix
 * @todo replace hardcoded strings with class variables
 * @todo add missing translation options 
 * 
 * @package WordPress
 * @subpackage Heylink Tracking
 */
class Heylink_Settings_Main {
  
	/**
	 * Plugin options from Database
	 * @var MIXED 
	 */
	public $options; 
	
	/**
	 * Load plugin options from the database.
	 * Hook into WordPress Dashboard screen adding a separate panel for Heylink Plugin.
	 */
	public function __construct() {
		$this->options = get_option(Heylink_Plugin::OPTIONS_NAME);
		
		add_action( 'admin_menu', array( $this, 'heylink_add_settings_page' ) );
		add_action( 'admin_init', array( $this, 'heylink_register_settings' ) );
		add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'heylink_tracker_settings_link' ) );
		add_action( 'admin_notices', array( $this, 'heylink_tracker_admin_notice_warn' ) );
	}

	/**
	 * Settings page main hook, the function adds_a_menu_page and runs related function.
	 * 
	 * @hook admin_menu
	 */
	public function heylink_add_settings_page() {
		add_menu_page( 'Heylink', 'Heylink', 'administrator', Heylink_Plugin::OPTIONS_PAGE_SUFFIX, array( $this, 'heylink_tracker_render_plugin_settings_page' ), 'dashicons-chart-area', 99 );
	}

	/**
	 * Redner the HTML of Heylink Plugin Settings page form. Use WordPress API's to renders settings sections 
	 * and validate them.
	 * 
	 * @hook add_menu_page
	 */
	public function heylink_tracker_render_plugin_settings_page() {
		?>
		<form action="options.php" method="post">
				<?php
				settings_fields( Heylink_Plugin::OPTIONS_NAME );
				do_settings_sections( 'heylink_tracker_plugin_page' );
				?>
				<input name="submit" class="button button-primary" type="submit" value="<?php esc_attr_e( 'Save' ); ?>" />
		</form>
		<?php
	}

	/**
	 * Register and add a number of plugin settings sections. Some of these are hidden and require
	 * an extra ?debug parameter added to the request.
	 * 
	 * @hook admin_init
	 */
	public function heylink_register_settings() {
		register_setting( 'heylink_tracker_plugin_options', 'heylink_tracker_plugin_options', array(
				'sanitize_callback' => null,
				'default' => null
		) );

		add_settings_section( 'api_settings', __( 'Heylink Tracking Settings', 'heylink-tracking' ), array( $this, 'heylink_tracker_plugin_section_text' ), 'heylink_tracker_plugin_page' );
		
		add_settings_field( 'heylink_tracker_plugin_setting_api_key', __( 'API Key', 'heylink-tracking' ), array( $this, 'heylink_tracker_plugin_setting_api_key' ), 'heylink_tracker_plugin_page', 'api_settings' );
		
		// hide some options by default
		if ( isset( $_GET['debug'] ) ) {
			add_settings_field( 'heylink_tracker_plugin_setting_results_debug', __( 'Debug', 'heylink-tracking' ), array( $this, 'heylink_tracker_plugin_setting_results_debug' ), 'heylink_tracker_plugin_page', 'api_settings' );
		}
		
		add_settings_field( 'heylink_tracker_plugin_setting_js_replace', __( 'Enable Heylink JS Tag', 'heylink-tracking' ), array( $this, 'heylink_tracker_plugin_setting_js_replace' ), 'heylink_tracker_plugin_page', 'api_settings' );

		add_settings_field( 'heylink_tracker_plugin_setting_method', __( 'Link optimization methods', 'heylink-tracking' ), array( $this, 'heylink_tracker_plugin_setting_method' ), 'heylink_tracker_plugin_page', 'api_settings' );
		
		add_settings_field( 'heylink_tracker_plugin_setting_ext_links', __( 'Rewrite as internal links', 'heylink-tracking' ), array( $this, 'heylink_tracker_plugin_setting_ext_links' ), 'heylink_tracker_plugin_page', 'api_settings' );

		add_settings_field( 'heylink_tracker_plugin_setting_ext_links_path', __( 'Path for internal links', 'heylink-tracking' ), array( $this, 'heylink_tracker_plugin_setting_ext_links_path' ), 'heylink_tracker_plugin_page', 'api_settings' );
		add_settings_field( 'heylink_tracker_plugin_setting_rel', __( 'Rel modifications', 'heylink-tracking' ), array( $this, 'heylink_tracker_plugin_setting_rel' ), 'heylink_tracker_plugin_page', 'api_settings' );
		
		// whiteliist / blacklist
		add_settings_field( 'heylink_tracker_plugin_setting_blacklist_whitelist_site', __( 'Blacklist or Whitelist Site', 'heylink-tracking' ), array( $this, 'heylink_tracker_plugin_setting_blacklist_whitelist_site' ), 'heylink_tracker_plugin_page', 'api_settings' );
		
		if( $this->options['blacklist_whitelist_site'] == 'blacklist' ) {
			add_settings_field( 'heylink_tracker_plugin_setting_blacklis', __( 'Blacklist', 'heylink-tracking' ), array( $this, 'heylink_tracker_plugin_setting_blacklist' ), 'heylink_tracker_plugin_page', 'api_settings' );
		}
	
		if( $this->options['blacklist_whitelist_site'] == 'whitelist' ) {
				add_settings_field( 'heylink_tracker_plugin_setting_whitelist', __( 'Whitelist', 'heylink-tracking' ), array( $this, 'heylink_tracker_plugin_setting_whitelist' ), 'heylink_tracker_plugin_page', 'api_settings' );
		}

		}

	/**
	 * Plugins settings page link, used under Plugins section of the Dashboard.
	 * 
	 * @hook plugin_action_links_
	 * @param string $links
	 * @return array
	 */
	public function heylink_tracker_settings_link( $links ) {

		$url = esc_url( add_query_arg(
										'page',
										Heylink_Plugin::OPTIONS_PAGE_SUFFIX,
										get_admin_url() . 'admin.php'
						) );

		$settings_link = "<a href='$url'>" . __( 'Settings', 'heyling-tracking' ) . '</a>';

		array_push(
						$links,
						$settings_link
		);

		return $links;
	}

	/**
	 * Option associated with external links. 
	 * Renders a radio, whether or not perform a redirect. 
	 * Hidden by default needs &debug flag added to the options page.
	 * 
	 * @hook heylink_tracker_plugin_setting_ext_links
	 */
	public function heylink_tracker_plugin_setting_ext_links() {
		
		$options = $this->options;
		
		$checked = '';
		if ( isset( $options['ext_links'] ) && $options['ext_links'] != '' ) {
			$checked = $options['ext_links'];
		}

		$one = ($checked == "1") ? 'checked="checked"' : "";
		$two = ($checked == "2") ? 'checked="checked"' : "";

		if ( $one == "" && $two == "" ) {
			$two = 'checked="checked"';
		}

		echo '<input type="radio" ' . $two . ' value="2" id="heylink_tracker_plugin_setting_ext_links" name="heylink_tracker_plugin_options[ext_links]">' . __( 'No', 'heylink-tracking' ) . '<br />
	  <input type="radio" ' . $one . ' value="1" id="heylink_tracker_plugin_setting_ext_links" name="heylink_tracker_plugin_options[ext_links]">' . __( 'Yes', 'heylink-tracking' );
	}

	/**
	 * Option associated with external links path, i.e. internal redirect path, say /go.
	 * Renders input text. Depends on the _ext_links method.
	 * 
	 * @hook heylink_tracker_plugin_setting_ext_links_path
	 */
	public function heylink_tracker_plugin_setting_ext_links_path() {
		$options = $this->options;

		$value = esc_attr( $options['ext_links_path'] );
		if ( $value == "" ) {
			$value = "go";
		}

		echo "<input id='heylink_tracker_plugin_setting_ext_links_path' name='heylink_tracker_plugin_options[ext_links_path]' type='text' value='" . $value . "' />";
	}

	/**
	 * Options for js replacemenet method. 
	 * Render a checkbox to check.
	 * 
	 * @hook heylink_tracker_plugin_setting_js_replace
	 */
	public function heylink_tracker_plugin_setting_js_replace() {

		$options = $this->options;
		$js_checked = '';

		if ( isset( $options['method_js'] ) && $options['method_js'] != '' ) {
			$js_checked = checked( 1, $options['method_js'], false );
		}

		echo '<input type="checkbox" ' . $js_checked . ' value="1" id="heylink_tracker_plugin_setting_method_js" name="heylink_tracker_plugin_options[method_js]">Enable the JS tag to get Pageviews and other Analytics data in Heylink.<br />';
	}

	/**
	 * Renders settings for output buffering or WordPress hooks.
	 * @todo: maybe change to a radio instead?
	 * 
	 * @hook heylink_tracker_plugin_setting_method
	 */
	public function heylink_tracker_plugin_setting_method() {
		$options = $this->options;

		$ob_checked = '';
		$hooks_checked = '';

		if ( isset( $options['method_ob'] ) && $options['method_ob'] != '' ) {
			$ob_checked = checked( 1, $options['method_ob'], false );
		}

		if ( isset( $options['method_hooks'] ) && $options['method_hooks'] != '' ) {
			$hooks_checked = checked( 1, $options['method_hooks'], false );
		}

		echo '<input type="checkbox" ' . $ob_checked . ' value="1" id="heylink_tracker_plugin_setting_method_ob" name="heylink_tracker_plugin_options[method_ob]">' . __( 'OB Cache - Scan all links when a page loads and replace with tracking links.', 'heylink-tracking' ) . '<br />';
		echo '<input type="checkbox" ' . $hooks_checked . ' value="1" id="heylink_tracker_plugin_setting_method_hook" name="heylink_tracker_plugin_options[method_hooks]">' . __( 'Wordpress Hooks - Add support for affiliate tracking to links made with PrettyLinks and similar internal redirect plugins.', 'heylink-tracking' );

		echo __( "<br /><br />Don't worry, Heylink only replaces links if affiliate campaigns are found for the specific link or if subId tracking is added for an existing affiliate link.", 'heylink-tracking' );
	}

	/**
	 * Validate and render the HTML of a settings for the rel attribute of dynamically created links.
	 * 
	 * @hook heylink_tracker_plugin_setting_rel
	 */
	public function heylink_tracker_plugin_setting_rel() {
		$options = $this->options;

		$rel_nofollow_checked = '';
		$rel_sponsored_checked = '';

		if ( isset( $options['rel_nofollow'] ) && $options['rel_nofollow'] != '' ) {
			$rel_nofollow_checked = checked( 1, $options['rel_nofollow'], false );
		}
		if ( isset( $options['rel_nofollow'] ) && $options['rel_nofollow'] == '' ) {
			$rel_nofollow_checked = 'checked="checked"';
		}

		if ( isset( $options['rel_sponsored'] ) && $options['rel_sponsored'] != '' ) {
			$rel_sponsored_checked = checked( 1, $options['rel_sponsored'], false );
		}
		if ( isset( $options['rel_sponsored'] ) && $options['rel_sponsored'] == '' ) {
			$rel_sponsored_checked = 'checked="checked"';
		}

		echo '<input type="checkbox" ' . $rel_nofollow_checked . ' value="1" id="heylink_tracker_plugin_setting_rel_nofollow" name="heylink_tracker_plugin_options[rel_nofollow]">rel="nofollow"<br />
	<input type="checkbox" ' . $rel_sponsored_checked . ' value="1" id="heylink_tracker_plugin_setting_rel_sponsored" name="heylink_tracker_plugin_options[rel_sponsored]">rel="sponsored"';
	}
	
	/**
	 * If checked the whole site will be blacklisted 
	 * and the whitelist textarea will reveal itself.
	 * 
	 * @hook heylink_tracker_plugin_setting_blacklist_site
	 */
	public function heylink_tracker_plugin_setting_blacklist_whitelist_site() {

		$options = $this->options;
		
		$checked = '';
		if ( isset( $options['blacklist_whitelist_site'] ) && $options['blacklist_whitelist_site'] != '' ) {
			$checked = $options['blacklist_whitelist_site'];
		}

		$default = ($checked == "default") ? 'checked="checked"' : "";
		$blacklist = ($checked == "blacklist") ? 'checked="checked"' : "";
		$whitelist = ($checked == "whitelist") ? 'checked="checked"' : "";

		if ( $blacklist == "" && $whitelist == "" ) {
			$default = 'checked="checked"';
		}
		
		echo 
		'<input type="radio" ' . $default . ' value="neither" id="heylink_tracker_plugin_setting_blacklist_whitelist_site" name="heylink_tracker_plugin_options[blacklist_whitelist_site]">' . __( 'Neither', 'heylink-tracking' ) . '<br />
		<input type="radio" ' . $blacklist . ' value="blacklist" id="heylink_tracker_plugin_setting_blacklist_whitelist_site" name="heylink_tracker_plugin_options[blacklist_whitelist_site]">' . __( 'Blacklist', 'heylink-tracking' ) . '<br />
	  <input type="radio" ' . $whitelist . ' value="whitelist" id="heylink_tracker_plugin_setting_blacklist_whitelist_site" name="heylink_tracker_plugin_options[blacklist_whitelist_site]">' . __( 'Whitelist', 'heylink-tracking' );
		?>
		<p class="heylink-setttings__desc"><?php _e( 'With blacklist activated, the plugin will run everywhere but the blacklisted entries. With whitelist activated the plugin will not run anywhere but the whitelisted entries.', 'heylink-tracking' ); ?></p>
		<?php
	}

	/**
	 * Print HTML of the blacklist setting.
	 * 
	 * @hook heylink_tracker_plugin_setting_blacklists
	 */
	public function heylink_tracker_plugin_setting_blacklist() {
		$options = $this->options;
		$blacklist = isset( $options['blacklist'] ) ? $options['blacklist'] : '';
		?>
		<select id="heylink_tracker_plugin_setting_blacklist" class="heylink-select heylink-select--blacklist" name="heylink_tracker_plugin_options[blacklist][]" multiple="multiple">'
				<?php if( !empty( $blacklist ) && is_array( $blacklist ) ): ?>
						<?php foreach( $blacklist as $blacklist_entry ): ?>
							<option value="<?php echo $blacklist_entry; ?>" selected>
								<?php echo get_the_title( $blacklist_entry ); ?>
							</option>
						<?php endforeach; ?>
				<?php endif; ?>
		</select>
		<?php
	}
	
	/**
	 * Print HTML of the whitelist setting.
	 * 
	 * @hook heylink_tracker_plugin_setting_blacklists
	 */
	public function heylink_tracker_plugin_setting_whitelist() {
		$options = $this->options;
		$whitelist = isset( $options['whitelist'] ) ? $options['whitelist'] : '';
		?>
		<select id="heylink_tracker_plugin_setting_whitelist" class="heylink-select heylink-select--whitelist" name="heylink_tracker_plugin_options[whitelist][]" multiple="multiple">'
				<?php if( !empty( $whitelist ) && is_array( $whitelist ) ): ?>
						<?php foreach( $whitelist as $whitelist_entry ): ?>
								<option value="<?php echo $whitelist_entry; ?>" selected>
										<?php echo get_the_title( $whitelist_entry ); ?>
								</option>
						<?php endforeach; ?>
				<?php endif; ?>
		</select>
		<?php
	}

	/**
	 * Unused, this is needed for the sake of API compatbility. 
	 * 
	 * @todo remove, or replace with something different
	 * 
	 * @hook heylink_tracker_plugin_section_text
	 */
	public function heylink_tracker_plugin_section_text() {
		// echo '<p>Here you can set all the options for using the API</p>';
	}


 /**
	* Validate and render the HTML of the API key settings.
	*  
	* @hook heylink_tracker_plugin_setting_api_key
	*/
	public function heylink_tracker_plugin_setting_api_key() {
		$options = $this->options;
		$api_key = isset( $options['api_key'] ) ? $options['api_key'] : '';
		echo "<input id='heylink_tracker_plugin_setting_api_key' name='heylink_tracker_plugin_options[api_key]' type='text' value='" . esc_attr( $api_key ) . "' />";
	}

	/**
	* Validate and render the HTML of the debug setting.
  *  
	* @hook heylink_tracker_plugin_setting_results_debug
	*/
	public function heylink_tracker_plugin_setting_results_debug() {
		$options = $this->options;
		$checked = '';
		if ( isset( $options['debug'] ) && $options['debug'] == 'on' ) {
			$checked = ' checked';
		}

		echo "<input id='heylink_tracker_plugin_setting_results_debug' name='heylink_tracker_plugin_options[debug]' type='checkbox'" . $checked . "/>";
		echo __( '<p>Debug information will be displayed in site footer</p>', 'heylink-tracking' );
	}

	/**
	 * Add admin notifications to to the top of the Back End screen if neeeded.
	 * @todo style these
	 * @todo make sure thes display only under Heylink Plugin 
	 * 
	 * @hook heylink_tracker_admin_notice_war
	 */
	public function heylink_tracker_admin_notice_warn() {
		$options = $this->options;
		if ( !isset( $options['api_key'] ) || $options['api_key'] == '' ):
			?>
			<div class="heylink-notice notice notice-warning is-dismissible">
				<p>
					<?php
							echo __( 'Please, add Heylink API key ', 'heylink-tracking' );
							echo '<a href="' . get_admin_url( null, 'admin.php?page=' . Heylink_Plugin::OPTIONS_PAGE_SUFFIX ) . '">'. _x( 'here', 'Notification link text', 'heylink-tracking' ) . '.</a>'; 
					?>
				</p>
			</div>
		<?php endif; 
		$heylink_tracker_plugin_clear_cache_notification = get_option( 'heylink_tracker_plugin_clear_cache_notification' );
		if ( $heylink_tracker_plugin_clear_cache_notification == 'show' ) {
			?>
		  <div class="heylink-notice notice notice-warning is-dismissible">
				<p>
						<?php
								echo __( 'Remember to clear your cache after making changes.', 'heylink-tracking' );
						?>
				</p>
			</div>
			<?php delete_option( 'heylink_tracker_plugin_clear_cache_notification' );
		}
	}

}

$heylink_settings_main_instance = new Heylink_Settings_Main();
