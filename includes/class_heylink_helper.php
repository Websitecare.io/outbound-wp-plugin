<?php

/**
 * A class of static helper methods. It contains useful commonly used functions statically. 
 *
 * @todo maybe access option names via Heylink_Plugin variable naame
 * 
 * @package WordPress
 * @subpackage Heylink Tracking
 */
class Heylink_Helper {

	/**
	 * Check if a provided string is an internal link.
	 * 
	 * @param string $link
	 * @return boolean
	 */
	public static function is_internal_link( $link ) {
		if ( strpos( $link, $_SERVER['HTTP_HOST'] ) == false ) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Check if the link is an anchor link,
	 * as in if it starts with a hash.
	 * 
	 * @param string $link
	 * @return boolean
	 */
	public static function is_anchor_link( $link ) {
		if ( strpos( $link, '#' ) === 0 ) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Check if provided string is a Heylink API link.
	 * 
	 * @param string $link
	 * @return boolean
	 */
	public static function is_heylink_link( $link ) {
		if ( strpos( $link, 'api.heylink.com' ) == false ) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Check if provided string is a Pretty Links plugin link,
	 * uses Pretty Links API.
	 * 
	 * @global prettylink object $prli_link
	 * @param string $a_href
	 * @return boolean
	 */
	public static function is_prli_link( $a_href ) {
		global $prli_link;
		$a_href_parsed = parse_url( $a_href );

		if ( isset( $prli_link ) and is_object( $prli_link ) and!empty( $a_href_parsed ) and isset( $a_href_parsed['path'] ) ) {
			if ( $prli_link->is_pretty_link( $a_href_parsed['path'], false ) ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Check if the site is blacklisted.
	 * 
	 * @return boolean
	 */
	public static function is_site_blacklisted() {
		$options = get_option( Heylink_Plugin::OPTIONS_NAME );
		if ( isset( $options['blacklist_whitelist_site'] ) and $options['blacklist_whitelist_site'] == 'blacklist' ) {
			return true;
		}
	}

	/**
	 * Check if the site is whitelisted.
	 * 
	 * @return boolean
	 */
	public static function is_site_whitelisted() {
		$options = get_option( Heylink_Plugin::OPTIONS_NAME );
		if ( isset( $options['blacklist_whitelist_site'] ) and $options['blacklist_whitelist_site'] == 'whitelist' ) {
			return true;
		}
	}

	/**
	 * Check if the currently viewed post / page / cpt is whitelisted
	 * @todo check if the page is added globally under settings
	 * @return boolean
	 */
	public static function is_current_entry_whitelisted() {
		global $post;
		if ( is_object( $post ) ) {
			return Heylink_Helper::is_whitelisted( $post->ID );
		}
		return false;
	}

	/**
	 * Check if the currently viewed post / page / cpt is whitelisted.
	 * 
	 * @todo check if the page is added globally under settings
	 * @return boolean
	 */
	public static function is_current_entry_blacklisted() {
		global $post;
		if ( is_object( $post ) ) {
			return Heylink_Helper::is_blacklisted( $post->ID );
		}
		return false;
	}

	/**
	 * Check if any provided ID is blacklisted based on post meta.
	 * Can be an id of a post, page, or a cpt.
	 * 
	 * @param int $post_id
	 */
	public static function is_blacklisted( $post_id ) {
		$options = get_option( Heylink_Plugin::OPTIONS_NAME );
		$blacklist = isset( $options['blacklist'] ) ? $options['blacklist'] : null;

		if ( isset( $blacklist ) ) {
			if ( in_array( $post_id, $blacklist ) ) {
				return true;
			}
		}

		return boolval( get_post_meta( $post_id, '_blacklist_check', true ) );
	}

	/**
	 * Check if any provided ID is whitelisted based on post meta.
	 * Can be an id of a post, page, or a cpt.
	 * 
	 * @param int $post_id
	 */
	public static function is_whitelisted( $post_id ) {
		$options = get_option( Heylink_Plugin::OPTIONS_NAME );
		$whitelist = isset( $options['whitelist'] ) ? $options['whitelist'] : null;

		if ( isset( $whitelist ) ) {
			if ( in_array( $post_id, $whitelist ) ) {
				return true;
			}
		}
		return boolval( get_post_meta( $post_id, '_whitelist_check', true ) );
	}

	/**
	 * Modify a link based on a number of parameters provided, return the modified URL.
	 * 
	 * @param string $link
	 * @param string $current_url
	 * @param boolean $ext_links 
	 * @param string $ext_links_path
	 * @param string $url_param
	 * @param string $page_url_param
	 * @param string $target_url_param
	 * @param string $hash_url_param
	 * @param string $hash_hmac_method
	 * @param string $hash_hmac_key
	 * @param string $heylink_tracking_url
	 * @param string $api_key
	 * 
	 * @return string modified url
	 */
	public static function modify_link( $link_href, $current_url, $ext_links, $ext_links_path, $url_param, $page_url_param, $target_url_param, $hash_url_param, $hash_hmac_method, $hash_hmac_key, $heylink_tracking_url, $api_key ) {
		
		if ( isset( $ext_links ) && $ext_links != 2 ) {
			$modified_link = 
							add_query_arg( array(
					$url_param => urlencode( $link_href ),
					$page_url_param => urlencode( $current_url ),
					$hash_url_param => hash_hmac( $hash_hmac_method, $link_href, $hash_hmac_key ),
											), site_url() . '/' . $ext_links_path
			);
		} else {
			$modified_link = esc_url(
							add_query_arg( array(
					$target_url_param => urlencode( htmlspecialchars_decode( html_entity_decode( urldecode( urlencode( $link_href ) ) ) ) ),
					$page_url_param => urlencode( $current_url ),
											), $heylink_tracking_url . '/' . $api_key
							)
			);
		}
		
		if ( Heylink_Helper::is_prli_link( $link_href ) ) {
			$modified_link = add_query_arg( array(
					'pri_link' => 'true',
											), $modified_link
							
			);
		}

		return $modified_link;
	}

	/**
	 * Compare two hash values provided.
	 * 
	 * @see https://developers.google.com/search/blog/2009/01/open-redirect-urls-is-your-site-being
	 * 
	 * @param string $hash_expected
	 * @param string $hash_value
	 * 
	 * @return boolean
	 */
	public static function validate_hash( $hash_expected, $hash_value ) {
		if ( !hash_equals( $hash_expected, $hash_value ) ) {
			return false;
		}
		return true;
	}

	/**
	 * Send a discover request to the heylink API
	 * Tests with cURL first, then file_get_contents if the server does not have cURL.
	 * 
	 * @param string URL
	 */
	public static function wrapped_file_get_contents( $url ) {
		if (
						function_exists( "curl_init" ) &&
						function_exists( "curl_setopt" ) &&
						function_exists( "curl_exec" ) &&
						function_exists( "curl_close" )
		) {
			$ch = curl_init();
			curl_setopt( $ch, CURLOPT_URL, $url );
			curl_setopt( $ch, CURLOPT_HTTPHEADER, array( 'Content-Type:application/json', 'User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36' ) );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_exec( $ch );
			curl_close( $ch );
		} else {
			if ( filter_var( ini_get( 'allow_url_fopen' ), FILTER_VALIDATE_BOOLEAN ) ) {
				file_get_contents( $url );
			}
		}
	}

	/**
	 * Get all blacklisted post / pages / custom post types.
	 * 
	 * @global MIXED $wpdb
	 */
	public static function get_all_blacklisted_entries() {
		global $wpdb;

		$blacklisted_entries = $wpdb->get_results(
						$wpdb->prepare( "SELECT post_id FROM {$wpdb->prefix}postmeta WHERE meta_key=%s AND meta_value=%d", array( '_blacklist_check', 1 ) ),
						'ARRAY_A'
		);

		return $blacklisted_entries;
	}

	/**
	 * Get all whitelisted post / pages / custom post types.
	 * 
	 * @global MIXED $wpdb
	 */
	public static function get_all_whitelisted_entries() {
		global $wpdb;

		$whitelisted_entries = $wpdb->get_results(
						$wpdb->prepare( "SELECT post_id FROM {$wpdb->prefix}postmeta WHERE meta_key=%s AND meta_value=%d", array( '_whitelist_check', 1 ) ),
						'ARRAY_A'
		);

		return $whitelisted_entries;
	}

}
