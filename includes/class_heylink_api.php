<?php
/**
 * A class that handles Heylink API connections and communication.
 * 
 * @link https://heylink.gitbook.io/outbound-api/api/channels
 * 
 * @package WordPress
 * @subpackage Heylink Tracking
 */
class Heylink_API {
	
	/**
	 * API link.
	 * 
	 * @var string
	 */
	public $api_link = 'https://api.heylink.com/api/outbound/';
	
	/**
	 * API key.
	 * 
	 * @var type 
	 */
	public $api_key = 'SljOsHQzgU86ZfAo';
	
	/**
	 * Channel API key.
	 * 
	 * @var type 
	 */
	public $channels_api_key = 'e7f1feeb-cd2a-46b1-9921-f363b9fc970c';
	
	/**
	* Setup API methods, call the API.
	*/
	public function __construct() {
			// var_dump( $this->get_available_channels() );
	}
	
	/**
	 * Get available channels from the API. 
	 * 
	 * @todo - incorrect API key
	 * 
	 * @return ARRAY
	 */
	public function get_available_channels() {
		
				$args = array(
						'headers' => array(
								'Content-Type' => 'application/json',
								'User-agent' => 'Heylink - WP Plugin Development',
								'Authorization' => 'Bearer ' . $this->channels_api_key,
						)
				);
				
				$url = $this->api_link . 'channels/v1/channels?limit=500&offset=0';
				
				$request = wp_remote_get( $url, $args );	
				$response_code = wp_remote_retrieve_response_code( $request );
				$response_body = wp_remote_retrieve_body( $request );
				
				return array( $response_code, $response_body );
	}
	
}

$heylink_api = new Heylink_API();
