<?php
/**
 * A class that handles AJAX requests of the plugin.
 * 
 * @todo test this solution on a site with lots of posts / pages / content - may need optimization improvements
 * 
 * @package WordPress
 * @subpackage Heylink Tracking
 */
class Heylink_AJAX {
	
	/**
	 * AJAX Endpoint name.
	 * 
	 * @see wp_ajax_
	 * @var string
	 */
	const AJAX_ENDPOINT = 'populate_select';
	
	/**
	 * Search param used to perform query searches.
	 * 
	 * @var string
	 */
	const QUERY_SEARCH_PARAM = 'search';
	
	/**
	 * Add a non public admin AJAX hooks that trigger the ajax request.
	 */
	public function __construct() {
		 // add AJAX action to logged in users only, accessible from admin only
		 if( is_admin() ) {
			 add_action( 'wp_ajax_' . Heylink_AJAX::AJAX_ENDPOINT, array( $this,  Heylink_AJAX::AJAX_ENDPOINT ) );
		 }
		 
	}
	
	/**
	 * Echoes a json array of values from the WordPress database, or an empty json array.
	 * 
	 * @hook wp_ajax_Heylink_AJAX::AJAX_ENDPOIN 
	 */
	public function populate_select() {
		
		$results = array();
		
		if( isset( $_GET[Heylink_AJAX::QUERY_SEARCH_PARAM] ) ) {
			$search_params = esc_attr( $_GET[Heylink_AJAX::QUERY_SEARCH_PARAM] );
		} else {
			exit;
		}
		
		$ajax_query_params = array(
				'post_type' => 'any',
				'posts_per_page' => -1,
				's' => $search_params,
		);
		
		$ajax_query = new WP_Query( $ajax_query_params );
		
		if( $ajax_query->have_posts() ):
			$results['results'] = array();
			while( $ajax_query->have_posts() ):
						$ajax_query->the_post();
						$results['results'][] = array(
								'id' => get_the_ID(),
								'text' => get_the_title(),
						);
			endwhile;
			wp_reset_postdata();
		endif;
	
		echo json_encode( $results );
		die;
	}
	
}

$heylink_ajax = new Heylink_AJAX();