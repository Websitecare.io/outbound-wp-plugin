# Outbound WP Plugin / Heylink Tracking

## Name
Heylink Tracking WordPress plugin

## Description
Connect your website to Heylink API through a number of different options to choose from. Automatically convert links to track the effectiveness of your links
without doing any manual code updates.

## Installation
Download the plugin files in a .zip format and extract the archive under the wp-content/plugins directory. Once done, log in to the Dashboard and Activate the Heylink Tracking plugin.

## Support
Reach out to support@heylink.com in case of problems with the plugin.

## License
GPLv2

## Project status
Under development.